#include<iostream>
#include <string>

class Animal // main class
{
private:
	std::string voice;
public:
	std ::string getvoice()
	{
		return voice;
	}
	virtual void ShowAnimalVoice()
	{
		std:: cout << voice;
	}
};


class Cow : public Animal 
{
private:
	std::string voice = "Moo\n";
public:
	std::string GetVoice()
	{
		return voice;
	}
	void ShowAnimalVoice() override
	{
		std::cout << voice;
	}
};

class Dog : public Animal
{
private:
	std::string voice = "Woof\n";
public:
	std::string GetVoice()
	{
		return voice;
	}
	void ShowAnimalVoice() override
	{
		std::cout << voice;
	}
};

int main()
{
	Animal* arr[2];
	arr[0] = new Dog;
	arr[1] = new Cow;
	for (int i = 0; i < 2; i++)
	{
		arr[i]->ShowAnimalVoice();
	}
	return 0;
}